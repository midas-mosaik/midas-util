# MIDAS Util

## Description

This package contains a set of utility functions that are used by other MIDAS packages. This includes the base data model and simulator that is used by most of the modules that are based on time used as well as the upgrade module base class.

## Installation

This package will usually be installed automatically if you use any of the other MIDAS packages. However, you can install it manually from pypi with

```bash
pip install midas-util
```

or clone this repository and install directly from the source code.

## Usage

Since this is a loose collection of functions and base classes, there is no general usage advice. If you want to use some of the functions, you should have a look at those functions. Most of them have at least a bit of documentation.

Further documentation is available at https://midas-mosaik.gitlab.io/midas.

## License

This software is released under the GNU Lesser General Public License (LGPL). See the license file for more information about the details. 
